---
--
--
--
---

function onInit()
	-- replace default CombatManager.getCombatantNodes()
	CombatManager.setCustomGetCombatantNodes(getCombatantNodes_sa);
	Token.onDelete = onTokenDelete;

	-- this lets us type "/saclean all" and remove any hidden entries we can't find.
	if User.isHost() then
		Comm.registerSlashHandler("saclean", processTokenCleaning);
		Comm.registerSlashHandler("sashow", processShowAll);
	end

end

function onClose()
end



--[[
	Set all entries in the combattracker.list as ACTIVe
	-useful for orphaned entries-
]]
function processShowAll(sCommand, sParams)
	if sParams == "all" then
		local aCTUnfilteredEntries = DB.getChildren("combattracker.list");
		for _,node in pairs(aCTUnfilteredEntries) do
			DB.setValue(node,"ct.visible","number",1);
		end
		ChatManager.SystemMessage("Flagging ALL combattracker entries as ACTIVE.");
	else
		ChatManager.SystemMessage("options: sashow [all]\Show ALL CT entries, hidden or otherwise.");
	end
end


--[[
	Remove ALL children in the combattracker, hidden or otherwise
]]
function processTokenCleaning(sCommand, sParams)
	if sParams == "all" then
		DB.deleteChildren('combattracker.list');
		ChatManager.SystemMessage("Deleted ALL combattracker entries.");
	else
		ChatManager.SystemMessage("options: saclean [all]\nRemoves ALL CT entries, hidden or otherwise entirely.");
	end
end

--[[
  Get a list of combatants from the CT that are supposed to be visible in the CT
  (and don't get the ones not flagged as in the CT)
]]
function getCombatantNodes_sa()
--Debug.console("manager_sa","getCombatantNodes_sa");
  local aCTUnfilteredEntries = DB.getChildren("combattracker.list");
  local aCTFilteredEntries = {};

  for _,node in pairs(aCTUnfilteredEntries) do
    local bCTVisible = (DB.getValue(node,"ct.visible",1) == 1);
    if bCTVisible then
      table.insert(aCTFilteredEntries,node);
    end
  end

  return aCTFilteredEntries;
end
  
--[[
	At some point we need to get ALL the tokens, not just the tokens
	marked as active. This function will get unfiltered list of all the 
	tokens in the same imageControl as this token
]]--
function getCTFromTokenUnfiltered(token)
	if not token then
		return nil;
	end
	
	local nodeContainer = token.getContainerNode();
	local nID = token.getId();

	return getCTFromTokenRefUnfiltered(nodeContainer, nID);
end
function getCTFromTokenRefUnfiltered(vContainer, nId)
	local sContainerNode = nil;
	if type(vContainer) == "string" then
		sContainerNode = vContainer;
	elseif type(vContainer) == "databasenode" then
		sContainerNode = vContainer.getNodeName();
	else
		return nil;
	end
	
	for _,v in pairs(DB.getChildren("combattracker.list")) do
		local sCTContainerName = DB.getValue(v, "tokenrefnode", "");
		local nCTId = tonumber(DB.getValue(v, "tokenrefid", "")) or 0;
		if (sCTContainerName == sContainerNode) and (nCTId == nId) then
			return v;
		end
	end
	
	return nil;
end

--[[
  activate/deActivateSelectedTokensInCT selected tokens in current imageControl 
]]
function activateSelectedTokensInCT(imageControl)
	manageActivationForSelectedTokens(imageControl,1);
end
function deActivateSelectedTokensInCT(imageControl)
	manageActivationForSelectedTokens(imageControl,0);
end
function manageActivationForSelectedTokens(imageControl,nActiveState)
	if imageControl then
		local aSelected = imageControl.getSelectedTokens();
		for _,nodeToken in pairs(aSelected) do
		  local nodeCT = getCTFromTokenUnfiltered(nodeToken);
			if nodeCT then
				if (ActorManager.isPC(nodeCT) and nActiveState == 0) then 
					-- had to stop dms from hiding pcs, to many people
					-- dont know what they did.
				else 
					DB.setValue(nodeCT,"ct.visible","number",nActiveState);
				end
				if nActiveState ~= 1 then
					DB.setValue(nodeCT,"tokenvis","number",0);
				else
					-- enabling token in CT, update effects widgets
					if TokenManager.bDisplayDefaultEffects then
						TokenManager.updateAttributesHelper(nodeToken, nodeCT);
					end
				end
			end
		end
	end
end
--[[
  Fired just before token is deleted
]]
function onTokenDelete(nodeToken)
	-- if a token is deleted from Image/MAP 
	-- lets make sure to flag the CT version of it visible if it's still there.
	-- Otherwise they'll have a lot of orphaned entries in the combattracker.lists.*
	local nodeCT = getCTFromTokenUnfiltered(nodeToken);
	if nodeCT then
	  DB.setValue(nodeCT,"tokenvis","number",1);
	end
	--
  end