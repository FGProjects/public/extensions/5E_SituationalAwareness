-- 
-- Please see the license.html file included with this distribution for 
-- attribution and copyright information.
--

function onInit()
	Debug.console("imagewindow_toolbar.lua","onInit");    
	update(true);
end

function update(bInit)
	super.update(bInit);

	if Session.IsHost then
		local image = getImage();
		local bHasTokens = image.hasTokens();
		toggle_ctvisible.setVisibility(bHasTokens);
		toggle_ctinvisible.setVisibility(bHasTokens);
	end
end
